# Golden Goblet Server (ggs)

## Links

* [API host](https://golden-goblet.herokuapp.com)
* [repo](https://gitlab.com/golden-goblet/backend)
* [public Docker registry](https://gitlab.com/golden-goblet/backend/container_registry)
* [Haddock documentation](https://golden-goblet.gitlab.io/backend/docs)
* [test coverage report](https://golden-goblet.gitlab.io/backend/coverage)
* [HLint report (if issues exist)](https://golden-goblet.gitlab.io/backend/hlint)
