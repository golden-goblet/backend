{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NamedFieldPuns #-}

module Data.Request.EntryNoKey where

import Data.Entities
import Data.Util

import Data.Int
import Data.Proxy
import GHC.Generics

import Data.Text (Text)
import Data.Time
import Data.Aeson
import Database.Persist

data EntryNoKey = EntryNoKey
	{ enkResult :: !Text
	, enkRank :: !(Maybe Int32)
	, enkPoints :: !(Maybe Double)
	, enkNote :: !(Maybe Text)
	, enkYoutubeVideo :: !(Maybe Text)
	, enkPublicAfter :: !(Maybe UTCTime)
	} deriving (Generic)

instance JsonOptions EntryNoKey where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "enk")
		, omitNothingFields = True
		}

instance FromJSON EntryNoKey where
	parseJSON = genericParseJSON $ jsonOptions @EntryNoKey Proxy

toEntryEntity :: EventId -> Int32 -> Text -> EntryNoKey -> Entity Entry
toEntryEntity
	eId
	match
	handle
	EntryNoKey
		{ enkResult
		, enkRank
		, enkPoints
		, enkNote
		, enkYoutubeVideo
		, enkPublicAfter
		}
	=
	Entity
		{ entityKey = EntryKey
			{ entryKeyeventId = eId
			, entryKeymatch = match
			, entryKeycontestantHandle = handle
			}
		, entityVal = Entry
			{ entryEventId = eId
			, entryMatch = match
			, entryContestantHandle = handle
			, entryResult = enkResult
			, entryRank = enkRank
			, entryPoints = enkPoints
			, entryNote = enkNote
			, entryYoutubeVideo = enkYoutubeVideo
			, entryPublicAfter = enkPublicAfter
			}
		}
