{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NamedFieldPuns #-}

module Data.Request.MatchNoKey where

import Data.Entities
import Data.Util

import Data.Int
import Data.Proxy
import GHC.Generics

import Data.Text (Text)
import Data.Aeson
import Database.Persist

data MatchNoKey = MatchNoKey
	{ mnkName :: !(Maybe Text)
	} deriving (Generic)

instance JsonOptions MatchNoKey where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "mnk")
		, omitNothingFields = True
		}

instance FromJSON MatchNoKey where
	parseJSON = genericParseJSON $ jsonOptions @MatchNoKey Proxy

toMatchEntity :: EventId -> Int32 -> MatchNoKey -> Entity Match
toMatchEntity
	eId
	match
	MatchNoKey
		{ mnkName
		}
	=
	Entity
		{ entityKey = MatchKey
			{ matchKeyeventId = eId
			, matchKeymatch = match
			}
		, entityVal = Match
			{ matchEventId = eId
			, matchMatch = match
			, matchName = mnkName
			}
		}
