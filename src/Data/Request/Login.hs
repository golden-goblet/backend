{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DeriveGeneric #-}

module Data.Request.Login where

import Data.Util

import Data.Proxy
import GHC.Generics

import Data.Text (Text)
import Data.Aeson

data Login = Login
	{ lUsername :: !Text
	, lPassword :: !Text
	} deriving (Generic)

instance JsonOptions Login where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "l")
		, omitNothingFields = True
		}

instance FromJSON Login where
	parseJSON = genericParseJSON $ jsonOptions @Login Proxy
