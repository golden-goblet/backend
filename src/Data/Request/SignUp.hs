{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DeriveGeneric #-}

module Data.Request.SignUp where

import Data.Util

import Data.Proxy
import GHC.Generics

import Data.Aeson
import Data.Text (Text)

data SignUp = SignUp
	{ suUsername :: !Text
	, suPassword :: !Text
	, suCode :: !Text -- TODO: should sign up be restricted with sign up codes?
	} deriving (Generic)

instance JsonOptions SignUp where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "su")
		, omitNothingFields = True
		}

instance FromJSON SignUp where
	parseJSON = genericParseJSON $ jsonOptions @SignUp Proxy
