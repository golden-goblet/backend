{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeApplications #-}

module Data.Request.ContestantRoles where

import Data.User
import Data.Entities
import Data.Util

import Data.Proxy
import GHC.Generics

import Data.Set (Set)
import Data.Aeson

data ContestantRoles = ContestantRoles
	{ crContestant :: ContestantId
	, crRoles :: !(Set Role)
	} deriving (Generic)

instance JsonOptions ContestantRoles where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "cr")
		, omitNothingFields = True
		}

instance FromJSON ContestantRoles where
	parseJSON = genericParseJSON $ jsonOptions @ContestantRoles Proxy
