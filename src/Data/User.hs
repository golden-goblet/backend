{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeApplications #-}

module Data.User where

import Data.Entities
import Data.Util

import Data.Int
import Data.Proxy
import GHC.Generics (Generic)

import Data.Set (Set)
import Data.Aeson
import Servant.Auth.Server

data User = User
	{ uContestant :: !ContestantId
	, uNonce :: !Int32
	, uRoles :: !(Set Role)
	} deriving (Show, Generic)

data Role
	= RoleTODO
	deriving (Show, Enum, Eq, Ord, Generic)

instance JsonOptions User where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ constructorTagModifier = camelTo2 '_' . drop (length @[] "u")
		}

instance JsonOptions Role where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ constructorTagModifier = camelTo2 '_' . drop (length @[] "Role")
		}

instance ToJSON User where
	toEncoding = genericToEncoding $ jsonOptions @User Proxy

instance FromJSON User where
	parseJSON = genericParseJSON $ jsonOptions @User Proxy

instance ToJSON Role where
	toEncoding = genericToEncoding $ jsonOptions @Role Proxy

instance FromJSON Role where
	parseJSON = genericParseJSON $ jsonOptions @Role Proxy

-- default instances use "dat" claim with a value using To/FromJSON
instance ToJWT User
instance FromJWT User

{-
For whatever reason manually making To/FromJWT does not work with servant auth
despite round tripping,
id = decodeJWT . encodeJWT

instance ToJWT User where
	encodeJWT User{ contestant = ContestantKey handle, nonce, roles } =
		emptyClaimsSet
			& claimSub ?~ review string handle
			& addClaim "nonce" (Number $ fromIntegral nonce)
			& addClaim "roles" (toJSON roles)

instance FromJWT User where
	decodeJWT claimsSet =
		-- tokens should not be modified externally so descriptive errors may not be necessary
		maybeToEither
			"missing fields (TODO? better error reporting)"
			do User
				<$> claimsSet^?claimSub._Just.string.to ContestantKey
				<*> claimsSet^?unregisteredClaims.at "nonce"._Just.to (parseMaybe @_ @Int32 parseJSON)._Just
				<*> claimsSet^?unregisteredClaims.at "roles"._Just.to (parseMaybe @_ @(Set Role) parseJSON)._Just
		where
			maybeToEither _ (Just a) = Right a
			maybeToEither e Nothing = Left e
-}
