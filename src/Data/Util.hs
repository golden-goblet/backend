module Data.Util where

import Data.Proxy

import Data.Aeson

class JsonOptions a where
	jsonOptions :: Proxy a -> Options
