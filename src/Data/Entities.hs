{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}

module Data.Entities where

import Data.Util

import Data.Int
import Data.Proxy
import GHC.Generics

import Data.Text (Text)
import Data.Time
import Data.Aeson
import Database.Persist.TH

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Game
	name Text
	UniqueGameName name
	deriving Show Generic

Event
	name Text
	game GameId
	startsOn UTCTime
	endsOn UTCTime
	matchesExpected Int32
	description Text Maybe
	deriving Show Generic

Match
	eventId EventId
	match Int32
	Primary eventId match

	name Text Maybe
	deriving Show Generic

Entry
	eventId EventId
	match Int32
	contestantHandle Text
	Foreign Match match_fk eventId match
	Foreign Contestant contestant_handle_fk contestantHandle
	Primary eventId match contestantHandle

	result Text
	rank Int32 Maybe
	points Double Maybe
	note Text Maybe
	youtubeVideo Text Maybe
	publicAfter UTCTime Maybe
	deriving Show Generic

Contestant
	handle Text
	Primary handle

	name Text Maybe
	youtubeChannel Text Maybe
	deriving Show Generic
|]


instance JsonOptions Game where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "game")
		, omitNothingFields = True
		}

instance JsonOptions Event where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "event")
		, omitNothingFields = True
	}

instance JsonOptions Match where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "match")
		, omitNothingFields = True
	}

instance JsonOptions Entry where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "entry")
		, omitNothingFields = True
	}

instance JsonOptions Contestant where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "contestant")
		, omitNothingFields = True
		}

instance ToJSON Game where
	toEncoding = genericToEncoding $ jsonOptions @Game Proxy

instance FromJSON Game where
	parseJSON = genericParseJSON $ jsonOptions @Game Proxy


instance ToJSON Event where
	toEncoding = genericToEncoding $ jsonOptions @Event Proxy

instance FromJSON Event where
	parseJSON = genericParseJSON $ jsonOptions @Event Proxy


instance ToJSON Match where
	toEncoding = genericToEncoding $ jsonOptions @Match Proxy

instance FromJSON Match where
	parseJSON = genericParseJSON $ jsonOptions @Match Proxy


instance ToJSON Entry where
	toEncoding = genericToEncoding $ jsonOptions @Entry Proxy

instance FromJSON Entry where
	parseJSON = genericParseJSON $ jsonOptions @Entry Proxy


instance ToJSON Contestant where
	toEncoding = genericToEncoding $ jsonOptions @Contestant Proxy

instance FromJSON Contestant where
	parseJSON = genericParseJSON $ jsonOptions @Contestant Proxy
