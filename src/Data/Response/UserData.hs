{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DeriveGeneric #-}

module Data.Response.UserData where

import Data.Util

import Data.Proxy
import GHC.Generics

import Data.Text (Text)
import Data.Aeson

data UserData = UserData
	{ udAccessToken :: Text
	--{ udAccessToken :: SignedJWT
	-- should be a signed JWT type to prevent stuffing any binary data you want
	-- however that requires a bit of work using jose and prevents the use of generic ToJSON
	-- which hampers eease of code gen for json decoders in other langs
	-- maybe revisit this choice in the future
	} deriving (Generic)

instance JsonOptions UserData where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "ud")
		, omitNothingFields = True
		}

instance ToJSON UserData where
	toEncoding = genericToEncoding $ jsonOptions @UserData Proxy
