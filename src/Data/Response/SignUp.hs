{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DeriveGeneric #-}

module Data.Response.SignUp where

import Data.Util

import Data.Proxy
import GHC.Generics

import Data.Aeson
import Data.Map (Map)
import Data.Text (Text)

data SignUp = SignUp
	{ suErrors :: Map Text Text -- TODO: consider better types
	} deriving (Generic)

instance JsonOptions SignUp where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "dee")
		, omitNothingFields = True
		}

instance ToJSON SignUp where
	toEncoding = genericToEncoding $ jsonOptions @SignUp Proxy
