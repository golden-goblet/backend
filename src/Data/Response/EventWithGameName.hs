{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NamedFieldPuns #-}

module Data.Response.EventWithGameName where

import Data.Entities
import Data.Util

import Data.Int
import Data.Proxy
import GHC.Generics

import Data.Text (Text)
import Data.Time
import Data.Aeson
import Database.Persist

data EventWithGameName = EventWithGameName
	{ ewgnEventId :: !EventId
	, ewgnEventName :: !Text
	, ewgnGameId :: !GameId
	, ewgnGameName :: !Text
	, ewgnEventStartsOn :: !UTCTime
	, ewgnEventEndsOn :: !UTCTime
	, ewgnMatchesExpected :: !Int32
	, ewgnMatchesCount :: !Int32
	, ewgnDescription :: !(Maybe Text)
	} deriving (Show, Generic)

instance JsonOptions EventWithGameName where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ constructorTagModifier = camelTo2 '_' . drop (length "Role")
		}

instance ToJSON EventWithGameName where
	toEncoding = genericToEncoding $ jsonOptions @EventWithGameName Proxy

toEventWithGameName :: Entity Event -> Text -> Int32 -> EventWithGameName
toEventWithGameName
	(Entity eventId Event{eventName, eventGame,  eventStartsOn, eventEndsOn, eventMatchesExpected, eventDescription})
	gameName
	matchCount
	= EventWithGameName
		{ ewgnEventId = eventId
		, ewgnEventName = eventName
		, ewgnGameId = eventGame
		, ewgnGameName = gameName
		, ewgnEventStartsOn = eventStartsOn
		, ewgnEventEndsOn = eventEndsOn
		, ewgnMatchesExpected =  eventMatchesExpected
		, ewgnMatchesCount = matchCount
		, ewgnDescription = eventDescription
		}
