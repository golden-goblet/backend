{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NamedFieldPuns #-}

module Data.Response.DetailedEvent where

import Data.Entities
import Data.Util

import Data.Int
import Data.Maybe
import Data.Proxy
import GHC.Generics

import Data.Aeson
import Data.Time
import Data.Text (Text)
import Database.Persist

data DetailedEvent = DetailedEvent
	{ deEventId :: !EventId
	, deEventName :: !Text
	, deGameId :: !GameId
	, deGameName :: !Text
	, deStartsOn :: !UTCTime
	, deEndsOn :: !UTCTime
	, deMatchesExpected :: !Int32
	, deDescription :: !(Maybe Text)
	, deMatchInfo :: ![DetailedEventMatch]
	, deContestantEntries :: ![DetailedEventContestantEntry]
	} deriving (Show, Generic)

data DetailedEventMatch = DetailedEventMatch
	{ demMatch :: !Int32
	, demName :: !Text
	} deriving (Show, Generic)

data DetailedEventContestantEntry = DetailedEventContestantEntry
	{ decmContestant :: !Contestant
	, decmEntries :: ![DetailedEventEntry]
	} deriving (Show, Generic)

data DetailedEventEntry = DetailedEventEntry
	{ deeMatch :: !Int32
	, deeResult :: !Text
	, deeRank :: !(Maybe Int32)
	, deePoints :: !(Maybe Double)
	, deeNote :: !(Maybe Text)
	, deeYoutubeVideo :: !(Maybe Text)
	} deriving (Show, Generic)

instance JsonOptions DetailedEvent where
	jsonOptions _  = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "de")
		, omitNothingFields = True
		}

instance JsonOptions DetailedEventMatch where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "dem")
		, omitNothingFields = True
		}

instance JsonOptions DetailedEventContestantEntry where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "decm")
		, omitNothingFields = True
		}

instance JsonOptions DetailedEventEntry where
	jsonOptions _ = Data.Aeson.defaultOptions
		{ fieldLabelModifier = camelTo2 '_' . drop (length "dee")
		, omitNothingFields = True
		}

instance ToJSON DetailedEvent where
	toEncoding = genericToEncoding $ jsonOptions @DetailedEvent Proxy

instance ToJSON DetailedEventMatch where
	toEncoding = genericToEncoding $ jsonOptions @DetailedEventMatch Proxy

instance ToJSON DetailedEventContestantEntry where
	toEncoding = genericToEncoding $ jsonOptions @DetailedEventContestantEntry Proxy

instance ToJSON DetailedEventEntry where
	toEncoding = genericToEncoding $ jsonOptions @DetailedEventEntry Proxy

toDetailedEvent :: Entity Event -> Entity Game -> [Match] -> [(Contestant, [Entry])] -> DetailedEvent
toDetailedEvent
	(Entity eventId
		Event{eventName, eventStartsOn, eventEndsOn, eventMatchesExpected, eventDescription}
	)
	(Entity gameId (Game gameName))
	matches
	contestantEntries
	= DetailedEvent
		{ deEventId = eventId
		, deEventName = eventName
		, deGameId = gameId
		, deGameName = gameName
		, deStartsOn = eventStartsOn
		, deEndsOn = eventEndsOn
		, deMatchesExpected = eventMatchesExpected
		, deDescription = eventDescription
		, deMatchInfo =  toDetailedEventMatches matches
		, deContestantEntries = toDetailedEventContestantEntries contestantEntries
		}

toDetailedEventMatches :: [Match] -> [DetailedEventMatch]
toDetailedEventMatches = fmap convert . filter (isJust . matchName) where
	convert Match{matchMatch, matchName} = DetailedEventMatch matchMatch $ fromJust matchName

toDetailedEventContestantEntries :: [(Contestant, [Entry])] -> [DetailedEventContestantEntry]
toDetailedEventContestantEntries = fmap (uncurry convert) where
	convert contestant entries = DetailedEventContestantEntry contestant $ toDetailedEventEntries entries

toDetailedEventEntries :: [Entry] -> [DetailedEventEntry]
toDetailedEventEntries = fmap convert where
	convert Entry{entryMatch, entryResult, entryRank, entryPoints, entryNote, entryYoutubeVideo} =
		DetailedEventEntry
			{ deeMatch = entryMatch
			, deeResult = entryResult
			, deeRank = entryRank
			, deePoints = entryPoints
			, deeNote = entryNote
			, deeYoutubeVideo = entryYoutubeVideo
			}
