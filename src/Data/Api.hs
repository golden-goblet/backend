{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Data.Api where

import Data.Entities
import Data.Pagination
import Data.User
import Data.Request as Req
import Data.Response as Resp

import Data.Int
import GHC.Generics

import Data.Text (Text)
import Servant
import Servant.API.Generic
import Servant.Auth

data Api route = Api
	{ publicApi :: route :- ToServant PublicApi AsApi
	, signUp    :: route :- "signup" :> ReqBody '[JSON] Req.SignUp :> Post '[JSON] Resp.SignUp
	, login     :: route :- "login" :> ReqBody '[JSON] Req.Login :> Post '[JSON] Resp.UserData
	, authApi   :: route :- Auth '[JWT] User :> ToServant AuthApi AsApi
	} deriving (Generic)

data PublicApi r = PublicApi
	{ detailedEvent
		:: r
		:- "detailed-event"
		:> Capture "event-id" EventId
		:> Get '[JSON] [Resp.DetailedEvent]
	, eventsDefault
		:: r
		:- "events"
		:> Get '[JSON] [Resp.EventWithGameName]
	, events
		:: r
		:- "events"
		:> QueryParam' '[Required, Strict] "sortby" Text
		:> QueryParam "sortfrom" Text
		:> QueryParam "order" SortOrder
		:> QueryParam "limit" Int32
		:> QueryParam "offset" Int32
		:> Get '[JSON] [Resp.EventWithGameName]
	, gamesDefault
		:: r
		:- "games"
		:> Get '[JSON] [Game]
	, games
		:: r
		:- "games"
		:> QueryParam' '[Required, Strict] "sortby" Text
		:> QueryParam "sortfrom" Text
		:> QueryParam "order" SortOrder
		:> QueryParam "limit" Int32
		:> QueryParam "offset" Int32
		:> Get '[JSON] [Game]
	, gameByName
		:: r
		:- "game"
		:> Capture "name" Text
		:> Get '[JSON] Game
	, gameById
		:: r
		:- "game"
		:> "id"
		:> Capture "id" Text
		:> Get '[JSON] Game
	} deriving (Generic)

data AuthApi r = AuthApi
	{ cmsApi :: r :- ToServant CmsApi AsApi
	, adminApi :: r :- "admin" :> ToServant AdminApi AsApi
	} deriving (Generic)

data CmsApi r = CmsApi
	{ createGame :: r :- "game" :> ReqBody '[JSON] Game :> Post '[JSON] GameId
	, updateGame :: r :- "game" :> Capture "id" GameId :> PutNoContent '[JSON] NoContent
	, createEvent :: r :- "event" :> ReqBody '[JSON] Game :> Post '[JSON] EventId
	, updateEvent :: r :- "event" :> Capture "id" EventId :> PutNoContent '[JSON] NoContent
	, createMatch :: r :- "match" :> ReqBody '[JSON] Match :> Post '[JSON] MatchId
	, updateMatch
		:: r
		:- "match"
		:> Capture "event" EventId
		:> Capture "match" Int32
		:> ReqBody '[JSON] Req.MatchNoKey
		:> PutNoContent '[JSON] NoContent
	, createEntry :: r :- "entry" :> ReqBody '[JSON] Entry :> Post '[JSON] EntryId
	, updateEntry
		:: r
		:- "entry"
		:> Capture "event" EventId
		:> Capture "match" Int32
		:> Capture "contestant" ContestantId
		:> ReqBody '[JSON] Req.EntryNoKey
		:> PutNoContent '[JSON] NoContent
	} deriving (Generic)

data AdminApi r = AdminApi
	{ grantRoles :: r :- "roles" :> ReqBody '[JSON] Req.ContestantRoles :> PutNoContent '[JSON] NoContent
	, removeRoles :: r :- "roles" :> ReqBody '[JSON] Req.ContestantRoles :> DeleteNoContent '[JSON] NoContent
	} deriving (Generic)
