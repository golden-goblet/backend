{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}

module Data.Pagination where

import Data.Int

import Data.Text
import Database.Persist
import Servant
import Web.HttpApiData

data Range entity a = Range
	{ rangeField ::  EntityField entity a
	, rangeValue :: Maybe a
	, rangeOrder :: SortOrder
	, rangeLimit :: Int32
	, rangeOffset :: Int32
	}


data SortOrder = Asc | Desc
	deriving (Show, Enum, Bounded)

instance FromHttpApiData SortOrder where
	parseQueryParam = parseBoundedTextData

instance (PersistEntity entity, Show a) => Show (Range entity a) where
	showsPrec d range =
		showParen (d > 10)
			$ showString "Range{rangeField = <"
			. showsPrec 0 (persistFieldDef $ rangeField range)
			. showString ">, rangeValue = "
			. showsPrec 0 (rangeValue range)
			. showString ", rangeOrder = "
			. showsPrec 0 (rangeOrder range)
			. showString ", rangeLimit = "
			. showsPrec 0 (rangeLimit range)
			. showString ", rangeOffset = "
			. showsPrec 0 (rangeOffset range)
			. showString "}"
