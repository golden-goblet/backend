{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE LambdaCase #-}

module Network.Server where

import Data.Api
import Data.User
import Network.Handlers
import Control.Database
import Control.Log

import Data.Proxy

import Servant.API.Generic
import Servant.Server
import Servant.Server.Generic
import Servant.Auth.Server
import Polysemy
import Polysemy.Error
import Polysemy.Input

routes
	:: Members '[DB, Error ServerError, Colog] r
	=> Api (AsServerT (Sem r))
routes = Api
	{ publicApi = toServant routesPublic
	, signUp = undefined
	, login = undefined
	, authApi = \case
		Authenticated user -> hoistServer (Proxy @(ToServant AuthApi AsApi)) (runInputConst user) $ toServant routesAuthApi
		_ -> throwAll err401
	}

-- orphan instance for servant-auth-server (TODO? move to it's own module and ignore orphans or ignore orphans here)
instance {-# OVERLAPPABLE #-} (Member (Error ServerError) r) => ThrowAll (Sem r a) where
	throwAll = throw

routesPublic
	:: Members '[DB, Error ServerError, Colog] r
	=> PublicApi (AsServerT (Sem r))
routesPublic = PublicApi
	{ detailedEvent = undefined
	, eventsDefault = undefined
	, events = undefined
	, gamesDefault = undefined
	, games = undefined
	, gameByName = undefined
	, gameById = undefined
	}

routesAuthApi
	:: Members '[DB, Error ServerError, Colog, Input User] r
	=> AuthApi (AsServerT (Sem r))
routesAuthApi = AuthApi
	{ cmsApi = toServant routesCmsApi
	, adminApi = toServant routesAdminApi
	}

routesCmsApi
	:: Members '[DB, Error ServerError, Colog, Input User] r
	=> CmsApi (AsServerT (Sem r))
routesCmsApi = CmsApi
	{ createGame = undefined
	, updateGame = undefined
	, createEvent = undefined
	, updateEvent = undefined
	, createMatch = undefined
	, updateMatch = undefined
	, createEntry = undefined
	, updateEntry = undefined
	}

routesAdminApi
	:: Members '[DB, Error ServerError, Colog, Input User] r
	=> AdminApi (AsServerT (Sem r))
routesAdminApi = AdminApi
	{ grantRoles = undefined
	, removeRoles = undefined
	}
