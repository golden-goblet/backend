{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}

module Network.Handlers where

{-
import Data.Entities
import Data.Request as Req
import Data.Response as Resp
import Control.Database
import Control.Log

import Data.Maybe
import Data.Proxy

import Servant
import Polysemy
import Polysemy.Error

getDetailedEvents
	:: Members '[DB, Colog] r
	=> Maybe (Ranges '["end_time"] DetailedEvent)
	-> Sem r (Headers (PageHeaders '["end_time"] DetailedEvent) [DetailedEvent])
getDetailedEvents ranges = do
	colog Debug "getting detailed events"
	let endTimeRange =
		fromMaybe (getDefaultRange $ Proxy @DetailedEvent) $ ranges >>= extractRange
	fetchRangeDetailedEventsByEndTime endTimeRange >>= returnRange endTimeRange

getEvents
	:: Members '[DB, Colog] r
	=> Maybe (Ranges '["end_time"] EventWithGameName)
	-> Sem r (Headers (PageHeaders '["end_time"] EventWithGameName) [EventWithGameName])
getEvents ranges = do
	colog Debug "getting events with game name"
	let endTimeRange =
		fromMaybe (getDefaultRange $ Proxy @EventWithGameName) $ ranges >>= extractRange
	fetchRangeEventWithGameNameByEndTime endTimeRange >>= returnRange endTimeRange

getEvent :: Members '[DB, Error ServerError, Colog] r => EventId -> Sem r Event
getEvent eId = do
	colog Debug "getting event by id"
	fetchEventById eId >>= note err404
-}
