{-# LANGUAGE OverloadedStrings #-}

module Network.Cors where

import Network.Wai
import Network.Wai.Middleware.Cors
import Network.HTTP.Types.Method
import Network.HTTP.Types.Header

requestToCorsPolicy :: Request -> Maybe CorsResourcePolicy
requestToCorsPolicy req = do
	origin <- lookup hOrigin $ requestHeaders req

	return CorsResourcePolicy
		{ -- TODO: evaluate if True (allow (require?) auth) should always be true
		  -- or if the request path should be parsed to a bool to decide if auth is necessary
		  corsOrigins = Just ([origin], True)
		, corsVaryOrigin = True
		, corsMethods = [methodGet, methodPost]
		, corsRequestHeaders = [hRange] -- servant pagination
		, corsExposedHeaders = Just $
			[ -- servant-pagination
			  "Accept-Ranges"
			, "Content-Range"
			, "Next-Range"
			] ++ simpleResponseHeaders
		, corsMaxAge = Nothing
		, corsRequireOrigin = False
		, corsIgnoreFailures = False
		}
