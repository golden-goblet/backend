{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Control.Log
	( colog
	, C.Severity (..)
	, C.runLogAction
	, C.richMessageAction

	, Colog
	, C.Log
	, C.log
	, C.Message
	, C.Msg

	, C.LogActionSem
	, C.runLogActionSem
	, C.runLogAsTrace
	, C.runLogAsOutput

	, C.runTraceAsLog
	, C.runOutputAsLog
	) where

import Data.Text
import Polysemy
import qualified Colog as C hiding (log)
import qualified Colog.Polysemy as C
import GHC.Stack

type Colog = C.Log C.Message

colog :: (HasCallStack, Member (C.Log C.Message) r) => C.Severity -> Text -> Sem r ()
colog sev msg = C.log $ C.Msg sev callStack msg
