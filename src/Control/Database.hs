{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Control.Database where

import Data.Entities
import Data.Response as Resp
import Data.Pagination
import Control.Log

import Data.Maybe
import qualified Data.Function as F
import qualified Data.List as List
import Data.Bifunctor
import Control.Monad

import qualified Data.Text as Text
import Polysemy
import Database.Esqueleto hiding (on, from)
import Database.Esqueleto.Experimental
import Data.Pool

data DB m a where
	FetchDetailedEvent :: EventId -> DB m (Maybe Resp.DetailedEvent)
	FetchRangeEventWithGameName :: (Show a, PersistField a) => Range Event a -> DB m [Resp.EventWithGameName]
	FetchEventById :: EventId -> DB m (Maybe Event)

makeSem ''DB

logDB :: Members '[Colog, DB] r => Sem r a -> Sem r a
logDB = intercept \case
	FetchDetailedEvent eId -> do
		colog Debug $ "fetching event with event id " <> do Text.pack $ show eId
		fetchDetailedEvent eId
	FetchRangeEventWithGameName range -> do
		colog Debug $ "fetching events with end time range " <> do Text.pack $ show range
		fetchRangeEventWithGameName range
	FetchEventById eId -> do
		let idStr = Text.pack $ show eId
		colog Debug $ "fetching event by id " <> idStr
		mEvent <- fetchEventById eId
		case mEvent of
			Nothing -> colog Debug $ "no event with id " <> idStr
			Just _ -> colog Debug $ "found event with id " <> idStr
		return mEvent

-- | Like 'Database.Esqueleto.associateJoin' except
-- the output is an association list instead of a map.
--
-- Input left entities are expected to be contiguous
-- such as the output from a SQL JOIN would produce.
--
-- Function maintains orginal list ordering.
--
-- Use when one only needs to traverse the resulting association list
-- and not for random lookup by the left entity key.
associateJoinListBy :: forall e1 e0 key. Eq key => (e0 -> key) -> [(e0, e1)] -> [(e0, [e1])]
associateJoinListBy toKey = fmap collapse . List.groupBy ((==) `F.on` (toKey . fst)) where
	collapse :: [(e0, e1)] -> (e0, [e1])
	collapse e01s = (leftEntity, rightEntities) where
		leftEntity = fst $ head e01s -- head is safe since groupBy wont generate inner empty lists
		rightEntities = fmap snd e01s

associateJoinList :: forall e1 e0. Eq (Key e0) => [(Entity e0, e1)] -> [(Entity e0, [e1])]
associateJoinList =  associateJoinListBy entityKey

runDBPersistPostgresqlPoolIOFinal :: Member (Final IO) r => Pool SqlBackend -> Sem (DB ': r) a -> Sem r a
runDBPersistPostgresqlPoolIOFinal pool = interpret \eff ->
	embedFinal @IO $ flip runSqlPool pool
		case eff of
			FetchDetailedEvent eId -> do
				eventGameAndContestantEntries <-
					select do
						(event :& game :& _ :& mEntry :&  mContestant) <-
							from $ Table @Event
							`InnerJoin` Table @Game
							`on` (\(event :& game) ->
								event ^. EventGame ==. game ^. GameId)
							`LeftOuterJoin` Table @Match
							`on` (\(event :& _ :& mMatch) ->
								just (event ^. EventId) ==. mMatch ?. MatchEventId)
							`LeftOuterJoin` Table @Entry
							`on` (\(_ :& _ :& mMatch :& mEntry) ->
								mMatch ?. MatchEventId ==. mEntry ?. EntryEventId
								&&. mMatch ?. MatchMatch ==. mEntry ?. EntryMatch)
							`LeftOuterJoin` Table @Contestant
							`on` (\(_ :& _ :& _ :& mEntry :& mContestant) ->
								mEntry ?. EntryContestantHandle ==. mContestant ?. ContestantHandle)
						where_ $ event ^. EventId ==. val eId
						orderBy [asc $ mEntry ?. EntryContestantHandle, asc $ mEntry ?. EntryMatch]
						return ((event, game), (mContestant, mEntry))

				let
					treeify
						:: [((Entity Event, Entity Game),
							(Maybe (Entity Contestant), Maybe (Entity Entry)))]
						-> [((Entity Event, Entity Game), [(Contestant, [Entry])])]
					treeify =
						fmap
							( second
								( fmap
									( second
										( fmap entityVal . catMaybes
										)
									)
								. treeifyContestant
								)
							)
						. treeifyEventGame where

						catMaybesOnFst =
							fmap (first fromJust)
							. filter (isJust . fst)
						treeifyEventGame =
							associateJoinListBy (entityKey . fst)
						treeifyContestant =
							fmap (first entityVal)
							. associateJoinList
							. catMaybesOnFst

					treeifiedEventGameAndContestantEntries = treeify eventGameAndContestantEntries
					eventIds = entityKey . fst . fst <$> treeifiedEventGameAndContestantEntries

				matchesByEvent <-
					select do
						(event :& match) <-
							from $ Table @Event
							`InnerJoin` Table @Match
							`on` (\(event :& match) ->
								event ^. EventId ==. match ^. MatchEventId)
						where_ $ event ^. EventId `in_` valList eventIds
						return match

				let
					treeifiedMatchesByEvent :: [[Match]]
					treeifiedMatchesByEvent = List.groupBy ((==) `F.on` matchEventId) $ fmap entityVal matchesByEvent

					convertToDetailedEvent
						:: [((Entity Event, Entity Game), [(Contestant, [Entry])])]
						-> [[Match]] -- contains groups of matches by event ids based on event ids of first arg (maybe missing, but never differnt ones) (sorted in same order as first arg)
						-> [DetailedEvent]
					convertToDetailedEvent eventGameContestantEntires [] =
						fmap toDetailedEventNoMatches eventGameContestantEntires
					convertToDetailedEvent
						(((eventEntity, gameEntity), contestantEntries):eventGameContestantEntires)
						matchesByevent@(matches:tailMatchesByEvent)
							-- head is safe because groupBy doesnt produce empty inner lists
							| entityKey eventEntity == matchEventId (head matches)
								= toDetailedEvent eventEntity gameEntity matches contestantEntries
									: convertToDetailedEvent eventGameContestantEntires tailMatchesByEvent
							-- eventIds of matchesByEvents may be missing so we assume empty matches
							-- and advance the eventGameContestantEntries list
							-- but lag the matchesByevent to catch up
							| otherwise
								= toDetailedEvent eventEntity gameEntity [] contestantEntries
									: convertToDetailedEvent eventGameContestantEntires matchesByevent
					convertToDetailedEvent [] _ = error "should never happen as left list is at  least as long as the right one"

					toDetailedEventNoMatches ((eventEntity, gameEntity), contestantEntries) =
						toDetailedEvent eventEntity gameEntity [] contestantEntries

				return $ listToMaybe $ convertToDetailedEvent treeifiedEventGameAndContestantEntries treeifiedMatchesByEvent

			FetchRangeEventWithGameName range -> do
				eventGameNameMatchCount <- select do
					(event :& game :& match) <-
						from $ Table @Event
						`InnerJoin` Table @Game
						`on` (\(event :& game) ->
							event ^. EventGame ==. game ^. GameId)
						`LeftOuterJoin` Table @Match
						`on` (\(event :& _ :& match) ->
							just (event ^. EventId) ==. match ?. MatchEventId)
					limit . fromIntegral $ rangeLimit range
					offset . fromIntegral $ rangeOffset range
					case rangeOrder range of
						Desc -> do
							orderBy [desc $ event ^. rangeField range]
							when (isJust $ rangeValue range) .
								where_ $ just (event ^. rangeField range) <=. val (rangeValue range)
						Asc -> do
							orderBy [asc $ event ^. rangeField range]
							when (isJust $ rangeValue range) .
								where_ $ just (event ^. rangeField range) >=. val (rangeValue range)
					let theGameName = game ^. GameName
					groupBy (event ^. EventId, theGameName)
					return (event, theGameName, count $ match ?. MatchMatch)
				let
					-- uncurry3
					convertToEventWithGameName (event, Value theGameName, Value matchCount) =
						toEventWithGameName event theGameName matchCount
				return $ fmap convertToEventWithGameName eventGameNameMatchCount

			FetchEventById eId ->
				get eId
