#!/usr/bin/env python
"""Hlint outputs a JSON object per line.
GitLab expects a JSON array of Code Climate issue JSON objects.

GitLab also only understands the simpler Location JSON schema
while Hlint uses the more verbose schema of it.
The conversion of JSON is the main reason to pull out python,
but converting lines of JSON objects to a JSON array of objects can also be done in python.

Reference to the subset of Code Climate issue JSON that GitLab understands and expects.
https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool
See also linked JSON spec, specificall JSON spec of locations
https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool
of which GitLab claims to understand only one (first link).
"""

import argparse
import json

parser = argparse.ArgumentParser(description='Convert code climate output of hlint to one GitLab understands.')
parser.add_argument('in_file')
parser.add_argument('out_file')

def main() -> None:
    args = parser.parse_args()

    cc_issues = []
    with open(args.in_file, 'r') as hlint_cc:
        for line in hlint_cc:
            # str.rstrip() does not seem to get rid of the new line characters
            # going to hope this is not that brittle
            line = line[:-2]
            cc_issue = json.loads(line)

            if (loc_positions := cc_issue['location'].get('positions')):
                cc_issue['location']['lines'] = {'begin': loc_positions['begin']['line'], 'end': loc_positions['end']['line']}
                del cc_issue['location']['positions']

            cc_issues.append(cc_issue)

    print(json.dumps(cc_issues, indent=4))

    with open(args.out_file, 'w') as gitlab_cc:
        json.dump(cc_issues, gitlab_cc)

if __name__ == '__main__':
    main()
