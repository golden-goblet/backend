{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}

module Test.Control.Database where

import Data.Entities
import Data.Response.DetailedEvent
import Control.Database

import Data.Time
import Polysemy
import Database.Persist.Sql


testEventEntity :: Entity Event
testEventEntity = Entity
	{ entityKey = toSqlKey 1
	, entityVal = Event
		{ eventName = "test name"
		, eventGame = toSqlKey 42
		, eventStartsOn = UTCTime (fromGregorian 2020 1 1) 0
		, eventEndsOn = UTCTime (fromGregorian 2021 1 1) 0
		, eventMatchesExpected = 13
		, eventDescription = Just "test description"
		}
	}

testGameEntity :: Entity Game
testGameEntity = Entity
	{ entityKey = toSqlKey 42
	, entityVal = Game
		{ gameName = "test game name"
		}
	}
runDBConst :: Sem (DB ': r) a -> Sem r a
runDBConst = interpret \case
	FetchDetailedEvent _ -> return . pure $ toDetailedEvent testEventEntity testGameEntity [] []
	FetchEventById (EventKey 11) -> return . Just $ entityVal testEventEntity
	FetchEventById _ -> return Nothing
