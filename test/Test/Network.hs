module Test.Network
	( Test.Network.tests
	) where

import Test.Network.Api

import Test.Tasty

tests :: TestTree
tests = testGroup "Network"
	[ Test.Network.Api.tests
	]
