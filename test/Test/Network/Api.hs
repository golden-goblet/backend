{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Test.Network.Api
	( Test.Network.Api.tests
	) where

import Data.Api
import Data.Entities
import Network.Server
import Test.Control.Database
import Control.Log

import qualified Data.ByteString.Char8 as BS
import Data.Time.Format.ISO8601
import Network.Wai
import Network.HTTP.Types
import Servant.Server
import Servant.Server.Generic
import Servant.Auth.Server
import Control.Monad.Trans.Except
import Polysemy
import Polysemy.Error
import Polysemy.Output
import Database.Persist (entityVal)
import Test.Tasty
--import Test.Tasty.ExpectedFailure
import Test.Tasty.Wai

app :: Application
app = genericServeTWithContext
	( Handler
	. ExceptT
	. runM @IO
	. runError @ServerError
	. ignoreOutput @Message
	. runLogAsOutput @Message
	. runDBConst
	)
	routes
	ctx
	where
		ctx = defaultCookieSettings :. defaultJWTSettings (fromSecret "1234567890987654321") :. EmptyContext


endTime :: BS.ByteString
endTime = urlEncode True . BS.pack . iso8601Show . eventEndsOn $ entityVal testEventEntity

tests :: TestTree
tests = testGroup "Api"
	[ testWai app "/event/11 GET 405" do
		res <- get "/event/11"
		assertStatus 405 res
	, testWai app "/event/42 GET 405" do
		res <- get "/event/7"
		assertStatus 405 res
	, testWai app "/detailed-events GET 404" do
		res <- get "/detailed-events"
		assertStatus 404 res
	, testWai app "/detailed-events GET 404 end_time 2020-08-08 08:20:00.123+0234; limit 3; order asc" do
		res <- srequest $ buildRequestWithHeaders
			GET
			"/detailed-events"
			""
			[("Range", "end_time " <> urlEncode True "2020-08-08 08:20:00.123+0234" <> ";limit 3; order asc")]
		assertStatus 404 res
	, testWai app "/detailed-events GET 404 end_time 2020-08-08T08:20:00Z" do
		res <- srequest $ buildRequestWithHeaders
			GET
			"/detailed-events"
			""
			[("Range", "end_time " <> urlEncode True "2020-08-08T08:20:00Z")]
		assertStatus 404 res
	]
