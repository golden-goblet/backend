module Main
	( main
	, topLevelTests
	, ingredients
	) where

import Test.Network

import Test.Tasty
import Test.Tasty.Ingredients
import Test.Tasty.Ingredients.Basic
import Test.Tasty.Runners.AntXML

main :: IO ()
main = defaultMainWithIngredients ingredients topLevelTests

ingredients :: [Ingredient]
ingredients =
	[ antXMLRunner `composeReporters` consoleTestReporter
	, listingTests
	]

topLevelTests :: TestTree
topLevelTests = testGroup "Test"
	[ Test.Network.tests
	]
