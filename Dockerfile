FROM fpco/stack-build:lts-16.5 AS builder

WORKDIR /opt/build

COPY stack.yaml stack.yaml.lock ggs.cabal /opt/build/
RUN stack build  --system-ghc --only-dependencies

COPY . /opt/build/
RUN stack install --system-ghc --local-bin-path /opt/build  ggs:ggs-exe


FROM ubuntu:18.04

WORKDIR /opt/ggs

RUN apt-get update && \
	apt-get upgrade -y --no-install-recommends && \
	apt-get install -y --no-install-recommends \
		libpq5 && \
	rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/sbin/pid1 /usr/local/sbin/pid1
COPY --from=builder /opt/build/ggs-exe /opt/ggs/ggs-exe

ENTRYPOINT ["/usr/local/sbin/pid1", "/opt/ggs/ggs-exe"]
