{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Data.Api
import Data.Entities
import Data.User
import Network.Server
import Network.Cors
import Control.Database
import Control.Log

import Control.Monad
import Control.Concurrent
import System.Exit

import qualified Data.Set as Set
import qualified System.Posix as Posix
import Servant
import Servant.Server.Generic
import Servant.Auth.Server
import Network.Wai.Handler.Warp
import Database.Persist.Sql
import Database.Persist.Postgresql
import Control.Monad.Logger
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import System.Environment
import Data.ByteString.Char8
import Polysemy
import Polysemy.Error
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Gzip
import Network.Wai.Middleware.Cors

main :: IO ()
main = do
	installSignalHandlers
	mDbUrl <- lookupEnv "DATABASE_URL"
	jwtKey <- generateKey
	print jwtKey
	let
		connStr = maybe "postgresql://test:password@localhost" pack mDbUrl
		jwtConfig = defaultJWTSettings jwtKey
		ctx = defaultCookieSettings :.  jwtConfig :. EmptyContext
	print =<< makeJWT (User (ContestantKey "some_handle") 1234  (Set.fromList [])) jwtConfig Nothing
	runStdoutLoggingT do
		withPostgresqlPool connStr 1 \pool -> do
			flip runSqlPool pool $ runMigration migrateAll

			liftIO
				. runEnv 8080
				. logStdout
				. gzip def
				. cors requestToCorsPolicy
				$ genericServeTWithContext
					( Handler
					. ExceptT
					. runFinal @IO
					. embedToFinal @IO
					. errorToIOFinal @ServerError
					. runLogAction @IO richMessageAction
					. runDBPersistPostgresqlPoolIOFinal pool
					. logDB
					)
					routes
					ctx

installSignalHandlers :: IO ()
installSignalHandlers = do
	mainThreadId <- myThreadId
	let
		sigTermHandler = Posix.Catch $ throwTo mainThreadId ExitSuccess
	void $ Posix.installHandler Posix.sigTERM sigTermHandler Nothing
